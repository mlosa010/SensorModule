#include <VL6180X.h>
#include <SparkFunBME280.h>
#include <stdint.h>
#include <Wire.h>

#define FORWARD 1
#define BACKWARD 0

// First module
#define DIR_PIN 4
#define PWM_PIN 5
#define ECO_PIN 3
#define OPT_PIN 12
#define ONE_STEP 150

/*
// Second module
#define DIR_PIN 5
#define PWM_PIN 6
#define ECO_PIN 2
*/

//declaring obj and its pinter ENV lidar
BME280 env;
VL6180X lidar;
BME280 * envAddy = &env;
VL6180X * disSensorAddy = &lidar;
volatile int counter=0;
void isr();


void info(){
  Serial.println("Instrumetation Module");
  Serial.println("FIU-ARC");
  Serial.println("Anthony & Many, Miami Summer 2018");
  Serial.println("---------------------------------");
  Serial.println(" p : pressure (Pa)");
  Serial.println(" t : temperature (Celcious)");
  Serial.println(" h : humidity (%)");
  Serial.println(" d : distance (mm)");
  Serial.println(" + : counterclockwise");
  Serial.println(" - : clockwise");
}

void setup(){
  attachInterrupt(digitalPinToInterrupt(ECO_PIN),isr,CHANGE);
  Wire.begin();
  //init I2C bus for ENV 
  env.settings.commInterface = I2C_MODE;
  env.settings.I2CAddress = 0x77;
  env.settings.runMode = 3; 
  env.settings.tStandby = 0;
  env.settings.filter = 0;
  env.settings.tempOverSample = 1;
  env.settings.pressOverSample = 1;
  env.settings.humidOverSample = 1;
  delay(10);  //Make sure  had enough time to turn on. BME280 requires 2ms to start up.         

  lidar.init();
  lidar.configureDefault();
  lidar.setTimeout(500);
  
  pinMode(ECO_PIN,INPUT);
  pinMode(OPT_PIN,OUTPUT);
  digitalWrite(OPT_PIN,HIGH);
  
  Serial.begin(9600);

  //info();
}
void loop(){
  Serial.println(lidar.readRangeSingleMillimeters());
   /*char command;
   char* commandAddy =&command;
    if (Serial.available() > 0) {
      // read the incoming byte:
      //*commandAddy = Serial.read();
      *commandAddy =Serial.read();
      switch (*commandAddy){
        case 'p':
          Serial.println("pressure is: ");
          Serial.println(envAddy->readFloatPressure());
          break;
         case 't':
          Serial.println("temperature in Celcious is: ");
          Serial.println(envAddy->readTempC());
          break;
         case 'h':
          Serial.println("the humidity is: ");
          Serial.println(envAddy->readFloatHumidity());
          break;
         case 'd':
          Serial.println("the distance in mm is: ");
          Serial.println(lidar.readRangeSingleMillimeters());
          break;
         case '+':
          analogWrite(PWM_PIN,160);
          digitalWrite(DIR_PIN,HIGH);
          delay(ONE_STEP);
          analogWrite(PWM_PIN,0);
          break;
         case '-':
          analogWrite(PWM_PIN,160);
          digitalWrite(DIR_PIN,LOW);
          delay(ONE_STEP);
          analogWrite(PWM_PIN,0);
          break;
         default:
          Serial.println("waiting for command");
      }
  }*/
  }

void isr() {
  counter++;
  Serial.println(counter);
}

